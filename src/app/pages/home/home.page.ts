import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(
    private as: AuthService,
    private mc: MenuController
  ) { }

  ngOnInit() {

    this.mc.enable(true)
  }

  logout(){
    this.as.logout()
  }
}
