import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { LoginService } from 'src/app/services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  hide_password = true;
  user: {
    email: string,
    pass: string
  } = {
      email: `laundrier@gmail.com`,
      pass: `123456`
    }
  constructor(
    private ls: LoginService,
    private nc: NavController,
    private mc: MenuController
  ) { }

  ngOnInit() {
    this.mc.enable(false)
  }

  hidePass() {
    this.hide_password = !this.hide_password;
  }

  loginNormal() {
    let is_valid = true;

    if (this.user.email.length <= 5 && !this.user.email.includes(`@`)) {
      console.log(`sini email`)
      is_valid = false;
    }
    if (this.user.pass.length < 6) {
      console.log(`sini pass`)

      is_valid = false;
    }
    if( this.user.pass.includes(`Delete`) ){
      console.log(`sini delete`)

      is_valid = false;
    }
    if( this.user.pass.includes(`DELETE`)) {
      console.log(`sini Det`)

      is_valid = false;
    }

    if (is_valid) {

      this.ls.loginWithEmailPassword(this.user.email, this.user.pass, res=>{this.nc.navigateRoot(`home`)})
    } else {
      
    }
  }
}
