import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, UrlTree } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AccessGuard implements CanLoad {

  constructor(
    private as: AuthService,
    private nc: NavController
  ) { }

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.as.authUser().then((res: any) => {

      if (res != null && res.email != null) {

        return true;
      }
      else {
        this.nc.navigateRoot(`login`)
        return false;
      }
    }).catch(err => {
      return false;
    })
  }

}
