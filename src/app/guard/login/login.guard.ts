import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, UrlTree } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanLoad {

  constructor(
    private as: AuthService,
    private nc: NavController
  ) { }


  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return this.as.authUser().then((res: any) => {

      if (res != null && res.email != null) {
        this.nc.navigateRoot(`home`)
        return false;
      }
      else {
        return true;
      }
    }).catch(err => {
      return true;
    })
  }

}
