import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import firebase from "firebase";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private nc: NavController
  ) { }

  authUser() {

    return new Promise((resolve, reject) => {

      firebase.auth().onAuthStateChanged(
        (user) => {

          resolve(user);
        },
        (err) => {
          reject(err);
        }
      );
    });

  }

  logout() {
    firebase.auth().signOut().then((res: any) => {
      this.nc.navigateRoot(`login`)
    });
  }

}
