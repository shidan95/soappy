import { Injectable } from '@angular/core';
import firebase from "firebase";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private rtdb = firebase.database();
  private fs = firebase.firestore();
  private auth = firebase.auth();  
  constructor() { }

  loginWithEmailPassword(email:string,pass:string, result=null){

    this.auth.signInWithEmailAndPassword(email, pass).then((res:any)=>{
      result(true)
    }).catch(err=>{
      result(false)
    })
  }
}
