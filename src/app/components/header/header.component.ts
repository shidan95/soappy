import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'comp-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() title:any = null;
  constructor() { }

  ngOnInit() {
    console.log(this.title)
  }

}
