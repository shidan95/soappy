// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBm7TRAhTNQCBshe79164Oi-dofxF4pVPQ",
    authDomain: "soapy-firebase.firebaseapp.com",
    projectId: "soapy-firebase",
    storageBucket: "soapy-firebase.appspot.com",
    messagingSenderId: "955498023827",
    appId: "1:955498023827:web:c9bba4ce67bf36d983b666",
    measurementId: "G-VXZPES2ZS4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
